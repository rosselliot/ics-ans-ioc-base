# ics-ans-ioc-base

Ansible playbook to setup the base requirements for an IOC.

## Requirements

- ansible >= 2.4
- molecule >= 2.6

## License

BSD 2-clause
